package chain

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ManagerHandler(var next1: Handler? = null, var next2: Handler? = null, var next3: Handler? = null): Handler {
    override fun handleRequest(messageToBeProcessed: String) {
        val words = messageToBeProcessed.split("-", ":")
        val messageType = words[0].trim()

        if (messageType == "Response") {
            // coroutine for next3 passing
            GlobalScope.launch {
                next3!!.handleRequest(messageToBeProcessed)
            }
            return
        }

        val priority = words[1].trim().toInt()
        val msg = words[2].trim()

        when (priority) {
            3 -> {
                // coroutine for message processing
                GlobalScope.launch {
                    println("Manager handling request $msg")
                    val response = "Response - good - $msg"
                    next1!!.handleRequest(response)
                }
            }
            else -> {
                // coroutine for next2 passing
                GlobalScope.launch {
                    next2!!.handleRequest(messageToBeProcessed)
                }
            }
        }
    }
    override fun setNexts(next1: Handler?, next2: Handler?, next3: Handler?) {
        this.next1 = next1
        this.next2 = next2
        this.next3 = next3
    }
    override fun prnt() {
        println("$this, ${this.next1}, ${this.next2}")
    }
}