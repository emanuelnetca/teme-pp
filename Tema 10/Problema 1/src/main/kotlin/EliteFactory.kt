package factory

import chain.CEOHandler
import chain.ExecutiveHandler
import chain.Handler
import chain.ManagerHandler

class EliteFactory: AbstractFactory() {
    override fun getHandler(handler: String): Handler? {
        when (handler) {
            "ceo" -> return CEOHandler()
            "exec" -> return ExecutiveHandler()
            "mgr" -> return ManagerHandler()
            else -> {
                println("No such handler")
                return null
            }
        }
    }
}