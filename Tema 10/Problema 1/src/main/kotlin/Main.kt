import factory.FactoryProducer
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() {
    val fp = FactoryProducer()
    val ef = fp.getFactory("elita")
    val hwf = fp.getFactory("worker")
    if (ef == null || hwf == null) {
        println("Something went wrong")
        return
    }

    val ceo1 = ef.getHandler("ceo")
    val ceo2 = ef.getHandler("ceo")
    val exec1 = ef.getHandler("exec")
    val exec2 = ef.getHandler("exec")
    val mgr1 = ef.getHandler("mgr")
    val mgr2 = ef.getHandler("mgr")

    val hw1 = hwf.getHandler("hw")
    val hw2 = hwf.getHandler("hw")
    if (ceo1 == null || ceo2 == null
        || exec1 == null || exec2 == null
        || mgr1 == null || mgr2 == null
        || hw1 == null || hw2 == null) {
        println("Something went wrong")
        return
    }

    ceo1.setNexts(ceo2, exec1, ceo1)
    ceo2.setNexts(ceo1, exec2, ceo1)
    exec1.setNexts(exec2, mgr1, ceo1)
    exec2.setNexts(exec1, mgr2, ceo2)
    mgr1.setNexts(mgr2, hw1, exec1)
    mgr2.setNexts(mgr1, hw2, exec2)
    hw1.setNexts(hw2, null, mgr1)
    hw2.setNexts(hw1, null, mgr2)

    ceo1.handleRequest("Request - 1:Ana are 5 mere.")
    ceo1.handleRequest("Request - 4:Cate mere are Ana?")
    ceo1.handleRequest( "Request - 2:5 mere Ana are!")
    ceo1.handleRequest("Request - 5:Potato!")
    ceo1.handleRequest("Request - 4:Msg")

    runBlocking {
        delay(5000)
    }
}