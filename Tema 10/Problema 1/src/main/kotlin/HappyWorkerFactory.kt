package factory

import chain.Handler
import chain.HappyWorkerHandler

class HappyWorkerFactory: AbstractFactory() {
    override fun getHandler(handler: String): Handler? {
        when (handler) {
            "hw" -> return HappyWorkerHandler()
            else -> {
                println("No such handler")
                return null
            }
        }
    }
}