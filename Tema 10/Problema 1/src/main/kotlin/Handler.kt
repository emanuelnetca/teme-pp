package chain

interface Handler {
    fun handleRequest(messageToBeProcessed: String)
    fun setNexts(next1: Handler?, next2: Handler?, next3: Handler?)
    fun prnt()
}