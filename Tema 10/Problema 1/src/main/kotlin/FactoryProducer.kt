package factory

class FactoryProducer {
    fun getFactory(choice: String): AbstractFactory? {
        if (choice == "elita") {
            return EliteFactory()
        }
        else if (choice == "worker") {
            return HappyWorkerFactory()
        }
        println("No such factory")
        return null
    }
}