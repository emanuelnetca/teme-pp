import java.io.File

fun encrypt(s: String, key: Int): String {
    val offset = key % 26
    if (offset == 0) return s
    var d: Char
    val chars = CharArray(s.length)
    for ((index, c) in s.withIndex()) {
        if (c in 'A'..'Z') {
            d = c + offset
            if (d > 'Z') d -= 26
        }
        else if (c in 'a'..'z') {
            d = c + offset
            if (d > 'z') d -= 26
        }
        else
            d = c
        chars[index] = d
    }
    return chars.joinToString("")
}

fun main() {
    val lineList = mutableListOf<String>()

    File("/home/manu/Desktop/PP/Laborator 12/Tema/Problema 2/src/example.txt").useLines { lines -> lines.forEach { lineList.add(
        it.split(" ").toString()
    ) } }
    println("\nTextul citit din fisier este: ")
    lineList.forEach { println(">  $it") }
    println("Textul criptat este: ")
    lineList.forEach { println(">  ${encrypt(it, 1)} ") }
}