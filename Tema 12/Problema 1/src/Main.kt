fun main(){
    val list = listOf(1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8)
    println("\nEliminarea numerelor <5:")
    val list1 = list.filter { it >= 5 }
    print(list1)
    println("\nGruparea numerelor in perechi:")
    val list2 = list1.zipWithNext()
    val list3 = list2.filter { list2.indexOf(it) % 2 == 0 }
    print(list3)
    println("\nInmultirea numerelor din perechi:")
    val list4 = list3.map {
        it.toList().first() * it.toList().last()
    }
    print(list4)
    println("\nSuma elementelor din lista:")
    val list5 = list4.sum()
    print(list5)
}