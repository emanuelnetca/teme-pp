import kotlin.math.pow
import kotlin.math.sqrt

fun main() {
    val myList = listOf(
        Pair(0, 0), // A
        Pair(0, 1), // B
        Pair(1, 0), // C
        Pair(1, 1) // D
    )
    val dist1 = sqrt(((myList[1].first - myList[0].first).toDouble()).pow(2) + ((myList[1].second - myList[0].second).toDouble()).pow(2))
    val dist2 = sqrt(((myList[3].first - myList[1].first).toDouble()).pow(2) + ((myList[3].second - myList[1].second).toDouble()).pow(2))
    val dist3 = sqrt(((myList[2].first - myList[3].first).toDouble()).pow(2) + ((myList[2].second - myList[3].second).toDouble()).pow(2))
    val dist4 = sqrt(((myList[0].first - myList[2].first).toDouble()).pow(2) + ((myList[0].second - myList[2].second).toDouble()).pow(2))
    println("\nDistantele: $dist1, $dist2, $dist3, $dist4")
    println("\nLista este: $myList")
    println("\nPerimetrul este: ${dist1 + dist2 + dist3 + dist4}")
}