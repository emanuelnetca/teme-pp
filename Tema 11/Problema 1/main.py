import asyncio

async def calculate_sum(nr):
    print(f"The sum for number {nr} is {sum(range(1,nr+1))}")

if __name__ == '__main__':
    list=[5,10,14,22]
    loop=asyncio.get_event_loop()
    loop.run_until_complete(
        asyncio.wait([calculate_sum(number) for number in list])
    )