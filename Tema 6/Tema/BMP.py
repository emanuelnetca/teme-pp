from Binary import Binary

class BMP(Binary):
    def __init__(self, path_absolut, frecvente, width, height, bpp):
        super().__init__(path_absolut, frecvente)
        self.width = width
        self.height = height
        self.bpp = bpp
    def show_info(self):
        print("width:{one}, height:{two}, bpp: {three}".format( one = self.width, two = self.height, three = self.bpp))
    def get_info(self) -> str:
        strg = self.get_path() + "\n\tis BMP file with:\n"
        strg += "\twidth = " + str(self.width) + " height = " + str(self.height) + " bpp = " + str(self.bpp)
        return strg