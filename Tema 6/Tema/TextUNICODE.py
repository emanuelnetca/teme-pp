from GenericFile import GenericFile

class TextUNICODE(GenericFile):
    def __init__(self, path_absolut, frecvente):
        self.path_absolut = path_absolut
        self.frecvente = frecvente
    def get_path(self) -> str:
        return self.path_absolut
    def get_freq(self) -> str:
        return self.frecvente
    def get_info(self) -> str:
        strg = self.get_path() + "\n\tis UNICODE text"
        return strg