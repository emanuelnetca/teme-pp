import os
from Binary import Binary
from TextASCII import TextASCII
from TextUNICODE import TextUNICODE
from XMLFile import XMLFile
from BMP import BMP
from GenericFile import GenericFile

def get_frequence(content) -> {}:
    frecv = {}
    for i in range(0, 256):
        frecv[i] = 0
    for ch in content:
        frecv[ch] += 1
    return frecv

def is_UNICODE(frecv, content_len) -> bool:
    if frecv[0] / content_len > 0.3:
        return 1
    return 0

def is_ASCII(frecv, content_len) -> bool:
    high_frec = [9, 10, 13]
    for i in range(32, 128):
        high_frec.append(i)

    high_frec_prob = 0
    for ch in frecv:
        if ch in high_frec:
            high_frec_prob += frecv[ch] / content_len
    low_frec_prob = 1 - high_frec_prob

    high_frec_prob /= len(high_frec)
    low_frec_prob /= (256 - len(high_frec))
    if high_frec_prob - low_frec_prob > 0.001:
        return 1
    return 0


def is_BINARY(frecv, content_len) -> bool:
    ideal_prob = content_len / 256
    delta = 0
    for elem in frecv:
        delta += abs(frecv[elem] - ideal_prob) / content_len
    if delta < 1:
        return 1
    return 0


def is_BMP(bmp) -> bool:
    magic = bmp.read(4).decode("UTF-8")
    if str(magic) != b'BM':
        return 0
    return 1

def is_XML(content) -> bool:
    nrBrackets = 0
    for ch in content:
        if ch == ord('<') or ch == ord('>'):
            nrBrackets += 1
    nrLines = len(content.splitlines())
    if nrBrackets / 2 > nrLines:
        return 1
    return 0


def XML_first_tag(content) -> str:
    start = content.find(ord('<'))
    if content[start + 1] == ord('?'):
        start = content.find(ord('<'), start + 1)
    end = content.find(ord('>'), start)

    tag1 = str(content[start + 1: end]).split(' ')[0]
    return tag1

def check_file_type(file_path) -> GenericFile:
    genFile = 0
    print("\nOpen file: ", file_path)
    f = open(file_path, 'rb')
    print("Opening ", file_path)
    content = f.read()
    frecv = get_frequence(content)
    if is_ASCII(frecv, len(content)):
        if is_XML(content):
            print('is_xml')
            first_tag = XML_first_tag(content)
            genFile = XMLFile(file_path, frecv, first_tag)
        else:
            print('is_ascii')
            genFile = TextASCII(file_path, frecv)
    if is_UNICODE(frecv, len(content)):
        print('is_unicode')
        genFile = TextUNICODE(file_path, frecv)
    if is_BINARY(frecv, len(content)):
        if is_BMP(f):
            print('is_bmp')
            genFile = BMP(file_path, frecv, -1, -1, -1)
        else:
            print('is_binary')
            genFile = Binary(file_path, frecv)
    f.close()
    return genFile;


if __name__ == '__main__':
    genFileList = []
    unknownList = []
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    for root, subdirs, files in os.walk(ROOT_DIR):
        for file in os.listdir(root):
            file_path = os.path.join(root, file)
            if os.path.isfile(file_path):
                newFile = check_file_type(file_path)
                if (newFile != 0):
                    genFileList.append(newFile)
                else:
                    unknownList.append(file_path)
    for string in unknownList:
        print("Unknown File: ", string, "\n")
    for index in range(0, len(genFileList)):
        print("[", index, "]: " + genFileList[index].get_info())