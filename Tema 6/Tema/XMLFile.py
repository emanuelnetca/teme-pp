from TextASCII import TextASCII

class XMLFile(TextASCII):
    def __init__(self, path_absolut, frecvente, firts_tag):
        TextASCII.__init__(self, path_absolut, frecvente)
        self.first_tag = firts_tag
    def get_first_tag(self) -> str:
        return self.first_tag
    def get_info(self) -> str:
        strg = self.get_path() + "\n\tis XML file with:\n"
        strg += "\tfirst_tag = {}".format(self.first_tag)
        return strg