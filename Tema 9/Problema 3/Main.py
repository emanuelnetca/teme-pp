import requests
from datetime import datetime


def time_func():
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    return dt_string


def transform_minutes(s):
    aux = s.split(":")
    return int(aux[0]) * 60 + int(aux[1]) + int(aux[2]) / 60


class RealURL:
    def __init__(self, url):
        self.url = url

    def request(self):
        with open("text", "a") as f:
            now = time_func()
            r = requests.get(self.url)
            f.write("Date: " + now + " URL: " + self.url + " Response: " + str(r.status_code) + "\n")


class Proxy:
    def __init__(self, real_url):

        self.real_url = real_url

    def request(self):
        url = self.real_url.url
        with open("text", "r") as f:
            d = f.readlines()
            for line in d:
                if url in line:
                    if transform_minutes(time_func()[11:]) - transform_minutes(line[17:25]) < 60:
                        print(f"Linia returnata este {line}")
                        break
                    else:
                        with open("text", "w") as o:
                            for ln in d:
                                if url not in ln.strip("\n"):
                                    o.write(ln)
                        print("Am gasit o intrare mai veche de o ora si trimit alt request")
                        self.real_url.request()


            else:
                print('Nu era o intrare asa ca o fac')
                self.real_url.request()


if __name__ == '__main__':
    url1 = RealURL("https://stackoverflow.com/questions/4710067/using-python-for-deleting-a-specific-line-in-a-file")
    url2 = RealURL("https://www.facebook.com")
    proxy = Proxy(url1)
    proxy.request()
    proxy2 = Proxy(url2)
    proxy2.request()
