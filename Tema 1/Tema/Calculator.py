# Programul Python pentru a crea un GUI simplu
# un calculator folosind Tkinter

# importam totul din modulul Tkinter
from tkinter import *

# declarația global a variabilei expression
expression = ""

# Funcție pentru actualizarea expresiei
# din caseta de introducere a textului
def press(num):
    # acceseaza variabila expression globală
    global expression

    # concatenarea șirului
    expression = expression + str(num)

    # actualizarea expresiei folosind metoda set
    equation.set(expression)

# Funcție de evaluare a expresiei finale
def equalpress():
    # Se folosește instrucțiunea try and except
    # pentru gestionarea erorilor.

    try:
        global expression

        # funcția eval evaluează expresia
        # și funcția str converteste rezultatul
        # în șir
        total = str(eval(expression))

        equation.set(total)

        # inițializează variabila expression
        # printr-un șir gol
        expression = ""
        # dacă eroarea este generată atunci se gestioneza
    # blocul except
    except:
        equation.set("error")
        expression = ""

# Funcție pentru stergerea continutului
# din caseta de introducere a textului
def clear():
    global expression
    expression = ""
    equation.set("")

# main
if __name__ == "__main__":
    # crearea fereastra GUI
    gui = Tk()

    # setarea culorii de fundal a ferestrei GUI
    gui.configure(background="gray")

    # setarea titlului ferestrei GUI
    gui.title("Silmple Calculator")

    # setare dimensiune fereastra GUI
    gui.geometry("236x149")

    # StringVar () este clasa variabilă
    # creăm o instanță a acestei clase
    equation=StringVar()

    # crearea casetei de introducere a textului pentru
    # afisarea expresiei.
    expression_field=Entry(gui, textvariable=equation)

    # metoda grid este utilizată pentru plasarea
    # widget-urile la pozițiile respective
    # în tabel ca structură
    expression_field.grid(columnspan=4, ipadx=55)

    equation.set('')

    # se creaza cate un buton la o anumita locatie
    # din fereastra rădăcină
    # când se apasă butonul, comanda sau
    # funcția aferentă butonului se executa functionalitatea butonului

    button1=Button(gui, text=' 1 ', fg='white', bg='gray', command=lambda: press(1), height=1, width=7)
    button1.grid(row=2, column=0)

    button2=Button(gui, text=' 2 ', fg='white', bg='gray', command=lambda: press(2), height=1, width=7)
    button2.grid(row=2, column=1)

    button3=Button(gui, text=' 3 ', fg='white', bg='gray', command=lambda: press(3), height=1, width=7)
    button3.grid(row=2, column=2)

    button4=Button(gui, text=' 4 ', fg='white', bg='gray', command=lambda: press(4), height=1, width=7)
    button4.grid(row=3, column=0)

    button5=Button(gui, text=' 5 ', fg='white', bg='gray', command=lambda: press(5), height=1, width=7)
    button5.grid(row=3, column=1)

    button6=Button(gui, text=' 6 ', fg='white', bg='gray', command=lambda: press(6), height=1, width=7)
    button6.grid(row=3, column=2)

    button7=Button(gui, text=' 7 ', fg='white', bg='gray', command=lambda: press(7), height=1, width=7)
    button7.grid(row=4, column=0)

    button8=Button(gui, text=' 8 ', fg='white', bg='gray', command=lambda: press(8), height=1, width=7)
    button8.grid(row=4, column=1)

    button9=Button(gui, text=' 9 ', fg='white', bg='gray', command=lambda: press(9), height=1, width=7)
    button9.grid(row=4, column=2)

    button0=Button(gui, text=' 0 ', fg='white', bg='gray', command=lambda: press(0), height=1, width=7)
    button0.grid(row=5, column=0)

    plus=Button(gui, text=' + ', fg='black', bg='orange', command=lambda: press("+"), height=1, width=7)
    plus.grid(row=2, column=3)

    minus=Button(gui, text=' - ', fg='black', bg='orange', command=lambda: press("-"), height=1, width=7)
    minus.grid(row=3, column=3)

    multiply=Button(gui, text=' * ', fg='black', bg='orange', command=lambda: press("*"), height=1, width=7)
    multiply.grid(row=4, column=3)

    divide=Button(gui, text=' / ', fg='black', bg='orange', command=lambda: press("/"), height=1, width=7)
    divide.grid(row=5, column=3)

    parantezaDeschisa=Button(gui, text=' ( ', fg='white', bg='gray', command=lambda: press("("), height=1, width=7)
    parantezaDeschisa.grid(row=5, column=1)

    parantezaInchisa=Button(gui, text=' ) ', fg='white', bg='gray', command=lambda: press(")"), height=1, width=7)
    parantezaInchisa.grid(row=5, column=2)

    clear=Button(gui, text=' Clear ', fg='black', bg='light gray', command=clear, height=1, width=7)
    clear.grid(row=6, column=1)

    equal=Button(gui, text=' = ', fg='black', bg='light gray', command=equalpress, height=1, width=7)
    equal.grid(row=6, column=2)

    # pornire GUI
    gui.mainloop()