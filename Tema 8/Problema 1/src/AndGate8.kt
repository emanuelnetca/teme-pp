class AndGate8 :GateAPI{
    public override fun calculate(entries: ArrayList<Boolean>) {
        val a=EntriesBuilder(entries)
        var result=true
        for(entry in a.values){
            result=result and entry
        }
        var context=Context()
        if(result==false){
            var s0=State0()
            s0.doAction(context)
        }else{
            var s1=State1()
            s1.doAction(context)
        }
        System.out.println("Poarta AND cu 8 intrari.")
        System.out.println("Rezultatul: "+context.getState())
    }
}