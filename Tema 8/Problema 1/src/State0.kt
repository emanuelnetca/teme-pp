class State0 :State{
    private val state:Boolean=false
    override fun doAction(context: Context) {
        context.setState(this)
    }

    public override fun toString(): String {
        return state.toString()
    }
}