class EntriesBuilder(val values:ArrayList<Boolean>) {
    public fun buildEntries():Entries{
        val entries=Entries()
        (0..values.size).forEach{
            entries.addEntry(Entry(values[it]))
        }
        return entries
    }
}   