class State1:State {
    private val state:Boolean=true
    override fun doAction(context: Context) {
        context.setState(this)
    }
    public override fun toString(): String {
        return state.toString()
    }
}