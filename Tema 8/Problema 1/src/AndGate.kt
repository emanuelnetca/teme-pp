class AndGate(val entries: ArrayList<Boolean>,gateAPI: GateAPI) :Gate(gateAPI) {
    override fun calculateOut() {
        gateAPI.calculate(entries)
    }
}