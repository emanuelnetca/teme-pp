class Context {
    private var state: State? =null
    public fun setState(state:State){
        this.state=state
    }
    public fun getState(): State? {
        return state
    }
}